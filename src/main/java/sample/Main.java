package sample;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.enums.AlignmentType;
import sample.enums.SequenceType;
import sample.enums.SubstitutionMatrixType;
import sample.models.SequenceInfo;

import java.io.File;

public class Main extends Application {
    Stage stage;
    Controller controller = new Controller();
    private ChoiceBox toolsBox;
    private ChoiceBox alignmentBox;
    private ChoiceBox matrixtBox;
    private CheckBox shouldUsePenaltyCheckBox;
    private TextField openGap;
    private TextField closeGap;

    @Override
    public void start(Stage primaryStage) throws Exception{
        VBox vbox = setupView();
        Scene scene = new Scene(vbox, 450, 300);
        stage = primaryStage;
        primaryStage.setScene(scene);
        primaryStage.setTitle("Projekt Bioinformatyka");
        primaryStage.show();
    }

    private VBox setupView() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(40, 10, 10, 30));
        vbox.setSpacing(25);
        HBox fileSection = createFileSection();
        HBox optionSection = createOptionSection();
        HBox penaltySection = createPenaltySection();
        HBox calculationSection = createCalculationSection();
        vbox.getChildren().addAll(fileSection, optionSection, penaltySection, calculationSection);
        return vbox;
    }

    private HBox createFileSection() {
        HBox hbox = new HBox();
        Label fileSectionTitle = new Label("Pick files: ");
        Button inputButton = new Button("Query sequence");
        FileChooser fileChooser = new FileChooser();
        inputButton.setOnAction(e -> {
            File selectedFile = fileChooser.showOpenDialog(stage);
            controller.handleQueryFile(selectedFile);
        });
        Button outPutButton = new Button("Target sequence");
        outPutButton.setOnAction(e -> {
            File selectedFile = fileChooser.showOpenDialog(stage);
            controller.handleTargetFile(selectedFile);
        });
        hbox.getChildren().addAll(fileSectionTitle, inputButton, outPutButton);
        hbox.setSpacing(20);
        return hbox;
    }

    private HBox createOptionSection() {
        HBox hbox = new HBox();
        Label optionSectionTitle = new Label("Pick options: ");
        toolsBox = createToolsChooser();
        alignmentBox = createAlignmentChooser();
        matrixtBox = createMatrixChooser();
        hbox.getChildren().addAll(optionSectionTitle, toolsBox, alignmentBox, matrixtBox);
        hbox.setSpacing(20);
        return hbox;
    }

    private ChoiceBox createToolsChooser() {
        ChoiceBox choiceBox = new ChoiceBox();
        choiceBox.getItems().add(SequenceType.dna);
        choiceBox.getItems().add(SequenceType.rna);
        choiceBox.getItems().add(SequenceType.protein);
        choiceBox.setValue(SequenceType.dna);

        return choiceBox;
    }

    private ChoiceBox createAlignmentChooser() {
        ChoiceBox choiceBox = new ChoiceBox();

        choiceBox.getItems().add(AlignmentType.global);
        choiceBox.getItems().add(AlignmentType.local);
        choiceBox.setValue(AlignmentType.global);
        return choiceBox;
    }
    private ChoiceBox createMatrixChooser() {
        ChoiceBox choiceBox = new ChoiceBox();

        choiceBox.getItems().add(SubstitutionMatrixType.blosum45);
        choiceBox.getItems().add(SubstitutionMatrixType.blosum62);
        choiceBox.getItems().add(SubstitutionMatrixType.blosum80);
        choiceBox.getItems().add(SubstitutionMatrixType.nuc4_2);
        choiceBox.getItems().add(SubstitutionMatrixType.nuc4_4);
        choiceBox.setValue(SubstitutionMatrixType.blosum45);
        return choiceBox;
    }

    private HBox createPenaltySection() {
        HBox hbox = new HBox();
        hbox.setSpacing(10);
        Label penaltySectionTitle = new Label("Penalty values: ");
        shouldUsePenaltyCheckBox = new CheckBox("Penalty");
        openGap = new TextField();
        closeGap = new TextField();
        openGap.setMaxWidth(50);
        closeGap.setMaxWidth(50);
        openGap.setText("0");
        closeGap.setText("1");
        hbox.getChildren().addAll(penaltySectionTitle, openGap, closeGap);
        return hbox;
    }

    private HBox createCalculationSection() {
        HBox hbox = new HBox();
        hbox.setSpacing(10);
        Button calculationButton = new Button("Perform alignment");
        calculationButton.setOnAction(e -> {
            this.updateController();
            try {
                this.controller.performAlignment();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        Button transcriptButton = new Button("Transcript");
        transcriptButton.setOnAction(e -> {
            this.updateController();
            try {
                this.controller.transcript();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Button multipleAlignmentButton = new Button("Multiple alignment");
        multipleAlignmentButton.setOnAction(e -> {
          this.controller.alignMultipleSequences();
        });
        hbox.getChildren().addAll(calculationButton, transcriptButton, multipleAlignmentButton);
        return hbox;
    }

    private void updateController() {
        int openGapValue = Integer.parseInt(openGap.getText());
        int closeGapValue = Integer.parseInt(closeGap.getText());
        boolean isPenaltyChecked = true;
        String sequenceType = (String)toolsBox.getValue();
        String alignmentType = (String)alignmentBox.getValue();
        String matrixBoxValue = (String)matrixtBox.getValue();
        SequenceInfo sequenceInfo = new SequenceInfo(openGapValue, closeGapValue, isPenaltyChecked, sequenceType, alignmentType, matrixBoxValue);
        controller.setSequenceInfo(sequenceInfo);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
