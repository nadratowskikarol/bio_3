package sample.enums;

public class SubstitutionMatrixType {
    //BLOSUM are used for sequence alignment of proteins
    //BLOSUM matrices with high numbers are designed for comparing closely related sequences,
    //while those with low numbers are designed for comparing distant related sequences

    //particularly long and weak alignments may provide the best results
    public static final String blosum45 = "BLOSUM-45";
    //one of the best solutions for detecting weak protein similarities
    public static final String blosum62 = "BLOSUM-62";
    //is used for alignments that are more similar in sequence
    public static final String blosum80 = "BLOSUM-80";


    //Only the first nucleotide sequence to align can contain ambiguous nucleotides
    public static final String nuc4_2 = "Nuc4_2";
    //Both of the nucleotide sequences to align can contain ambiguous nucleotides
    public static final String nuc4_4 = "Nuc4_4";




}
