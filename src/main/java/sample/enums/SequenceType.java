package sample.enums;

public class SequenceType {
    public static final String dna = "DNA";
    public static final String rna = "RNA";
    public static final String protein = "PROTEIN";

}
