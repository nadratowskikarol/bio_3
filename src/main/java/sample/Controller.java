package sample;

import org.biojava.nbio.alignment.NeedlemanWunsch;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.SmithWaterman;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import sample.enums.AlignmentType;
import sample.enums.SequenceType;
import sample.models.SequenceInfo;
import sample.services.*;

import java.io.File;

public class Controller {
    AlignmentPerformer alignmentPerformer;
    File queryFile;
    File targetFile;
    SequenceInfo sequenceInfo;

    public Controller() {
        alignmentPerformer = new AlignmentPerformer();
    }

    public void handleQueryFile(File selectedFile) {
        this.queryFile = selectedFile;
    }

    public void handleTargetFile(File selectedFile) {
        this.targetFile = selectedFile;
    }

    public String performAlignment() throws Exception {
        if (queryFile == null || targetFile == null) {
            System.out.println("Controller [performAlignment()] File is not chosen!");
            return "choose file";
        }

        String query = SequenceReader.getSequenceFromFile(queryFile.getPath());
        String target = SequenceReader.getSequenceFromFile(targetFile.getPath());
        String sequenceType = sequenceInfo.getSequenceType();
        String alignmentType = sequenceInfo.getAlignmentType();
        SimpleGapPenalty penalty = new SimpleGapPenalty(sequenceInfo.getOpenGap(), sequenceInfo.getCloseGap());

        if (sequenceType.equals(SequenceType.protein)) {
            SubstitutionMatrix<AminoAcidCompound> substitutionMatrix = ScoringMatrixProvider.getSubstitutionMatrixForAmino(sequenceInfo.getMatrixType());
            if (alignmentType.equals(AlignmentType.global)) {
                alignmentPerformer.performGlobalAlignmentWithNeedlemanWunschAlgorithm(query, target, sequenceType, substitutionMatrix, penalty);
            } else {
                alignmentPerformer.performLocalAlignment(query, target, sequenceType, substitutionMatrix, penalty);
            }
        } else {
            SubstitutionMatrix<NucleotideCompound> substitutionMatrix = ScoringMatrixProvider.getSubstitutionMatrixForNuc(sequenceInfo.getMatrixType());
            if (alignmentType.equals(AlignmentType.global)) {
                alignmentPerformer.performGlobalAlignmentWithNeedlemanWunschAlgorithm(query, target, sequenceType, substitutionMatrix, penalty);
            } else {
                alignmentPerformer.performLocalAlignment(query, target, sequenceType, substitutionMatrix, penalty);
            }
        }
        return "";
    }

    public void alignMultipleSequences() {
        try {
            MultipleSequenceAligner.multipleSequenceAlignment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void transcript() throws Exception {
        if (queryFile == null ) {
            System.out.println("Controller [transcript()] File is not chosen!");
            return;
        }

        String query = SequenceReader.getSequenceFromFile(queryFile.getPath());
        String sequenceType = sequenceInfo.getSequenceType();
        if (sequenceType.equals(SequenceType.rna)) {
            SequenceTranscriptor.transcriptRNAToProtein(query);
        } else if (sequenceType.equals(SequenceType.dna)) {
            SequenceTranscriptor.transcriptDNAToRNA(query);
        }
    }

    public void setSequenceInfo(SequenceInfo sequenceInfo) {
        this.sequenceInfo = sequenceInfo;
    }
}
