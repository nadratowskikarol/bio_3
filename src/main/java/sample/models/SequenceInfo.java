package sample.models;

public class SequenceInfo {
    int openGap;
    int closeGap;
    boolean isPenaltyChecked;
    String sequenceType;
    String alignmentType;
    String matrixType;

    public SequenceInfo(int openGap, int closeGap, boolean isPenaltyChecked, String sequenceType, String alignmentType, String matrixType) {
        this.openGap = openGap;
        this.closeGap = closeGap;
        this.isPenaltyChecked = isPenaltyChecked;
        this.sequenceType = sequenceType;
        this.alignmentType = alignmentType;
        this.matrixType = matrixType;
    }

    public int getOpenGap() {
        return openGap;
    }

    public void setOpenGap(int openGap) {
        this.openGap = openGap;
    }

    public int getCloseGap() {
        return closeGap;
    }

    public void setCloseGap(int closeGap) {
        this.closeGap = closeGap;
    }

    public boolean isPenaltyChecked() {
        return isPenaltyChecked;
    }

    public void setPenaltyChecked(boolean penaltyChecked) {
        isPenaltyChecked = penaltyChecked;
    }

    public String getSequenceType() {
        return sequenceType;
    }

    public void setSequenceType(String sequenceType) {
        this.sequenceType = sequenceType;
    }

    public String getAlignmentType() {
        return alignmentType;
    }

    public String getMatrixType() {
        return matrixType;
    }

    public void setMatrixType(String matrixType) {
        this.matrixType = matrixType;
    }

    public void setAlignmentType(String alignmentType) {
        this.alignmentType = alignmentType;
    }
}
