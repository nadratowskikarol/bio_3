package sample.models;

public class PenaltyCost {
    short match;
    short replace;
    short insert;
    short delete;
    short gapExtend;

    public PenaltyCost(short match,  short replace,  short insert,  short delete,  short gapExtend) {
        this.match = match;
        this.replace = replace;
        this.insert = insert;
        this.delete = delete;
        this.gapExtend = gapExtend;
    }

}
