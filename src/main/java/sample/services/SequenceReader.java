package sample.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

// class used to load sequence from file
public class SequenceReader {

    public static String getSequenceFromFile(String path) throws Exception {
        File file = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder sequence = new StringBuilder();
        String st;
        while ((st = br.readLine()) != null)
            sequence.append(st);
        return sequence.toString();
    }

}
