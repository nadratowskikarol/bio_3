package sample.services;

import org.biojava.nbio.alignment.NeedlemanWunsch;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.SmithWaterman;
import org.biojava.nbio.alignment.routines.GuanUberbacher;
import org.biojava.nbio.alignment.template.GapPenalty;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Compound;
import sample.enums.SequenceType;
import sample.models.PenaltyCost;

import javax.sound.midi.Sequence;
import java.io.File;

public class AlignmentPerformer {
    public void performGlobalAlignmentWithGuanUberbacherAlgorithm(String query, String target, String sequenceType, SubstitutionMatrix matrix, GapPenalty penalty) throws CompoundNotFoundException {
        if (sequenceType == SequenceType.dna) {
            DNASequence querySequence = new DNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            DNASequence targetSequence = new DNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            GuanUberbacher<DNASequence, NucleotideCompound> aligner = new GuanUberbacher<DNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<DNASequence, NucleotideCompound> alignment = aligner.getPair();
        } else if (sequenceType == SequenceType.rna) {
            RNASequence querySequence = new RNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            RNASequence targetSequence = new RNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            GuanUberbacher<RNASequence, NucleotideCompound> aligner = new GuanUberbacher<RNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<RNASequence, NucleotideCompound> alignment = aligner.getPair();
        }
        ProteinSequence querySequence = new ProteinSequence(query, new AminoAcidCompoundSet());
        ProteinSequence targetSequence = new ProteinSequence(target, new AminoAcidCompoundSet());
        GuanUberbacher<ProteinSequence, AminoAcidCompound> aligner = new GuanUberbacher<ProteinSequence, AminoAcidCompound>(querySequence, targetSequence, penalty, matrix);
        SequencePair<ProteinSequence, AminoAcidCompound> alignment = aligner.getPair();
    }

    public void performGlobalAlignmentWithNeedlemanWunschAlgorithm(String query, String target, String sequenceType, SubstitutionMatrix matrix, GapPenalty penalty) throws CompoundNotFoundException {
        if (sequenceType.equals(SequenceType.dna)) {
            DNASequence querySequence = new DNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            DNASequence targetSequence = new DNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            NeedlemanWunsch<DNASequence, NucleotideCompound> aligner = new NeedlemanWunsch<DNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<DNASequence, NucleotideCompound> alignment = aligner.getPair();
            int identicals = alignment.getNumIdenticals();
            int similars = alignment.getNumSimilars();
            int x = 0;
        } else if (sequenceType.equals(SequenceType.rna)) {
            RNASequence querySequence = new RNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            RNASequence targetSequence = new RNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            NeedlemanWunsch<RNASequence, NucleotideCompound> aligner = new NeedlemanWunsch<RNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<RNASequence, NucleotideCompound> alignment = aligner.getPair();
        }
        ProteinSequence querySequence = new ProteinSequence(query, new AminoAcidCompoundSet());
        ProteinSequence targetSequence = new ProteinSequence(target, new AminoAcidCompoundSet());
        NeedlemanWunsch<ProteinSequence, AminoAcidCompound> aligner = new NeedlemanWunsch<ProteinSequence, AminoAcidCompound>(querySequence, targetSequence, penalty, matrix);
        SequencePair<ProteinSequence, AminoAcidCompound> alignment = aligner.getPair();
    }

    public void performLocalAlignment(String query, String target, String sequenceType, SubstitutionMatrix matrix, GapPenalty penalty) throws CompoundNotFoundException {
        if (sequenceType.equals(SequenceType.dna)) {
            DNASequence querySequence = new DNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            DNASequence targetSequence = new DNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            SmithWaterman<DNASequence, NucleotideCompound> aligner = new SmithWaterman<DNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<DNASequence, NucleotideCompound> alignment = aligner.getPair();
            int identicals = alignment.getNumIdenticals();
            int similars = alignment.getNumSimilars();
            int x = 0;
        } else if (sequenceType.equals(SequenceType.rna)) {
            RNASequence querySequence = new RNASequence(query, AmbiguityDNACompoundSet.getDNACompoundSet());
            RNASequence targetSequence = new RNASequence(target, AmbiguityDNACompoundSet.getDNACompoundSet());
            SmithWaterman<RNASequence, NucleotideCompound> aligner = new SmithWaterman<RNASequence, NucleotideCompound>(querySequence, targetSequence, penalty, matrix);
            SequencePair<RNASequence, NucleotideCompound> alignment = aligner.getPair();
        }
        ProteinSequence querySequence = new ProteinSequence(query, new AminoAcidCompoundSet());
        ProteinSequence targetSequence = new ProteinSequence(target, new AminoAcidCompoundSet());
        SmithWaterman<ProteinSequence, AminoAcidCompound> aligner = new SmithWaterman<ProteinSequence, AminoAcidCompound>(querySequence, targetSequence, penalty, matrix);
        SequencePair<ProteinSequence, AminoAcidCompound> alignment = aligner.getPair();
    }


}
