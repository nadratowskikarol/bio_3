package sample.services;

import org.biojava.nbio.core.alignment.matrices.SubstitutionMatrixHelper;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import sample.enums.SubstitutionMatrixType;

public class ScoringMatrixProvider {

    public static SubstitutionMatrix<AminoAcidCompound> getSubstitutionMatrixForAmino(String matrixType) {
        if (matrixType.equals(SubstitutionMatrixType.blosum45)) {
            return SubstitutionMatrixHelper.getBlosum45();
        } else if (matrixType.equals(SubstitutionMatrixType.blosum62)) {
            return SubstitutionMatrixHelper.getBlosum62();
        } else if (matrixType.equals(SubstitutionMatrixType.blosum80)) {
            return SubstitutionMatrixHelper.getBlosum80();
        }
        return SubstitutionMatrixHelper.getBlosum80();
    }

    public static SubstitutionMatrix<NucleotideCompound> getSubstitutionMatrixForNuc(String matrixType) {
       if (matrixType.equals(SubstitutionMatrixType.nuc4_2)) {
            return SubstitutionMatrixHelper.getNuc4_2();
        }
        return SubstitutionMatrixHelper.getNuc4_4();
    }
}
