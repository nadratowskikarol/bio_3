package sample.services;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;

public class SequenceTranscriptor {

    public static void transcriptRNAToProtein(String rnaString) throws CompoundNotFoundException {
        RNASequence rnaSequence = new RNASequence(rnaString, AmbiguityDNACompoundSet.getDNACompoundSet());
        ProteinSequence proteinSequence = rnaSequence.getProteinSequence();
        System.out.printf("Protein: %s", proteinSequence.toString());
    }

    public static void transcriptDNAToRNA(String dnaString) throws CompoundNotFoundException {
        DNASequence dnaSequence = new DNASequence(dnaString, AmbiguityDNACompoundSet.getDNACompoundSet());
        RNASequence rnaSequence = dnaSequence.getRNASequence();
        System.out.printf("RNA: %s", rnaSequence.toString());
    }
}
